const Product = require("../models/Product")
const bcrypt = require("bcrypt")
const auth = require("../auth")
const User = require("../models/User")



//create product
module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newProduct.save().then((product, error) => {

		if (error) {
			return false
		} else {
			return true
		}
	})
}





//retrieve all products
module.exports.getAllProducts = () => {
	return Product.find({isActive: true}).then(result =>{
		return result;
	})
}



//retrieve single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		let product = {
			name: result.name,
			description: result.description,
			price:result.price
		}
		return product;
	})
}

//update product
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
	.then((product,error) => {
		if(error){
			return "Unsuccessful updating of product information."
		
		} else {
			return "Successfully updated product information"
		}
		})
}


//update product information / archiving
module.exports.archiveProduct = (reqParams, reqBody) => {
    
    return Product.findByIdAndUpdate(reqParams.productId).then(result => {
        
        if (result == null) {
            return "Cannot find product ID."
        
        } else { result.isActive = false
            return result.save().then((success, saveErr) => {
               
                if (saveErr) {
                    console.log(saveErr)
                    return "Archiving not complete."

                } else {
                    return "Archived Successfully."
                }
            })
        }
    })

}





















