const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},
	
	orders:
		[
			{
				productId: 
				{
					type: String,
					required: [true, 'Enter your product ID.']
				},

				totalAmount: {
					type: Number,
					
				},

				purchasedOn: {
					type: Date,
					default: new Date(),
				},

				status: {
					type: String,
					default: "purchased"
				}
			}
		]

	

})

module.exports = mongoose.model("User", userSchema);
