const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, 'insert name of product']
	},

	description: {
		type: String,
		required: [true, 'description of the product']
	},

	price: {
		type: Number,
		required: [true, 'insert price']
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {

		type: Date,
		default: new Date()
	},

	customers: 
		[
			{
				userId: 
				{
					type: String,
					required: [true, 'User ID is required']
				},

				purchasedOn: {
					type: Date,
					default: new Date()
				}
			}
		]
})


module.exports = mongoose.model('Product', productSchema)