import {Row, Col, Card} from 'react-bootstrap'

export default function Highlights() {
	return(
	<Row className = "mt-3 mb-3">
		<Col xs = {12} md = {4} >
			<Card className = "cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						<h2>Everyday SALE. Up to 70% OFF on selected items</h2>
					</Card.Title>
					<Card.Text>
						Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illo aliquid magnam repellendus quia ex perferendis cupiditate nemo, doloremque iure tempore perspiciatis hic nulla quas adipisci ipsum veritatis laboriosam natus nesciunt.
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>
		<Col xs = {12} md = {4}>
			<Card className = "cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						<h2>Flexible installment packages</h2>
					</Card.Title>
					<Card.Text>
						Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illo aliquid magnam repellendus quia ex perferendis cupiditate nemo, doloremque iure tempore perspiciatis hic nulla quas adipisci ipsum veritatis laboriosam natus nesciunt.
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>
		<Col xs = {12} md = {4}>
			<Card className = "cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						<h2>Contact Us</h2>
					</Card.Title>
					<Card.Text>
						Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illo aliquid magnam repellendus quia ex perferendis cupiditate nemo, doloremque iure tempore perspiciatis hic nulla quas adipisci ipsum veritatis laboriosam natus nesciunt.
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>
	</Row>
	)
}