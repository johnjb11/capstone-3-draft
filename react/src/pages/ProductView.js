import {useState, useEffect, useContext} from 'react'
import {Container, Row, Col, Card, Button} from 'react-bootstrap'
import {useParams, useNavigate, Link} from 'react-router-dom'
// useHistory for v5 down in react-router-dom
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function ProductView (){

	// useParams hook allows us to retrieve the courseId passed via the URL.
	const {productId} = useParams()

	const {user} = useContext(UserContext)

	// useHistory or useNavigate hook allows us to redirect user to a different page
	const history = useNavigate()

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0)


	const createOrder = (productId) => {
		fetch(`http://localhost:4000/users/createOrder`, {
			method: 'POST',
			headers : {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId,
				userId: user.id
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: 'Order created Successfully!',
					icon: 'success',
					text: 'You have successfully ordered this product.'
				})

				history.push("/products")

			} else {
				Swal.fire({
					title: 'Something went wrong!',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})
	}

	useEffect(() => {
		console.log(productId)

		fetch(`http://localhost:4000/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	}, [productId])



	return(
		<Container className = "mt-5">
			<Row>
				<Col lg = {{span: 6, offset: 3}}>
					<Card>
						<Card.Body className = "text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text> Php {price}</Card.Text>
							
							{user.id !== null ?
								<Button variant = "primary" onClick = {() => createOrder(productId)}>Add to cart</Button>
								:
								<Link className = "btn btn-danger btn-block" to = "/login">Log in to buy</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}