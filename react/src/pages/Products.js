import {Fragment, useEffect, useState} from 'react'
// import coursesData from '../data/coursesData'
import ProductCard from '../components/ProductCard'

export default function Products(){
	
	// console.log(coursesData)
	// console.log(coursesData[0])

const [products, setProduct] = useState([])

useEffect(() => {
	fetch('http://localhost:4000/products/all')
	.then(res => res.json())
	.then(data => {
		console.log(data)
	
	setProduct(data.map(product => {
	return (
		<ProductCard key = {product.id} productProp = {product}/>
	)
}))


	})
}, [])

// const courses = coursesData.map(course => {
// 	return (
// 		<CourseCard key = {course.id} courseProp = {course}/>
// 	)
// })


	return(
	<Fragment>	
		<h1>Products</h1>
		{products}
	</Fragment>	
	)
}